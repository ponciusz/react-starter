

const initialState = {
    fetching: false,
    fetched: false,
    error: null
};

const compare = (state = initialState, action) => {

    switch (action.type) {

        case "MODAL_SHOW":
            return {
                ...state,
                modal: {
                    modalType: action.modalType,
                    modalProps: action.modalProps
                }
            };

        default:
            return state;
    }
};

export default compare

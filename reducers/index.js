import { combineReducers } from 'redux'
import compare from './compare'

const reducers = combineReducers({
    compare
});

export default reducers

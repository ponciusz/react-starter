import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import App from "./src/Components/App";
import store from "./store";

if (process.env.NODE_ENV !== "production") {
  console.log("Looks like we are in development mode!");
}

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
